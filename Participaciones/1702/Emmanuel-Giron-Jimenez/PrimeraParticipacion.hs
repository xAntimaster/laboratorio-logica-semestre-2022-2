module PrimeraParticipacion where 

uno = [1]

dos = [1,2]

tres = [1,2,3]

cuatro = [1,2,3,4]

--Lista de listas
list = [uno, uno, tres, dos, tres, cuatro, dos, uno]

--Ingresa una lista l donde sus elementos son listas. Devuelve una lista de listas solo de tamaño n . 
listasDeN l n = [xs | xs <- l, length xs == n]

--Concatena listas de l que el tamaño de sus elementos sean pares.
dobleListaComp l = [ xs++xs | xs <- l, (length xs) `mod` 2 == 0]
