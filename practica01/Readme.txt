Integrantes:
- Emmanuel Girón Jimenez.
- Alejandro Blancas Peralta.


Para está práctica nos pusimos a aprender Haskell, ya que ninguno de nosotros sabía como programar aquí.
De igual manera sucedió con GitLab, si bien a este aún no le agarramos la onda tratamos de hacer el esfuerzo.
Tratamos de hacer llamada de vez en cuando, cuando había alguna duda importante.
Al principio nos costó un poco entender bien que había que hacer en cada función, pero las ayudantías nos
hicieron comprender un poco mejor que deberíamos hacer.
Una funcion que por ejemplo nos costó por lo mismo del lenguaje fue BinPosToInt, pues si bien teniamos la noción
de como debería ser la conversión temas como la multiplicación de la base por el exponente era un tema que no era tan fácil
de ver. Si bien no pudimos resolver del todo bien esa parte vamos tratando de progresar en el aprendizaje de este lenguaje.