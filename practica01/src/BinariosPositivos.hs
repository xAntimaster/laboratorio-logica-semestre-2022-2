--Equipo: 
--Emmmanuel Giron Jimenez
--Alejandro Blancas Peralta 

module BinariosPositivos where 

data BinPos = U | Cero (BinPos) | Uno (BinPos) deriving(Eq)

--Instancia de la clase Show para representar nuestros números binarios positivos.
instance Show BinPos where
    show U = "1"
    show (Cero b) = show b ++ "0"
    show (Uno b) = show b ++ "1"

--Función que dado un BinPos devuelva a su sucesor.
sucesor  :: BinPos -> BinPos
sucesor U = (Cero U) -- 1 -> 10
sucesor (Cero b) = (Uno b) -- 0 -> 1
sucesor (Uno b)  = (Cero (sucesor b)) -- 1 -> 10
{-ejemplos:
    -> sucesor (Cero (Uno (Cero (Uno (Cero U))))) -> representa al número 42
    Resultado: 101011 -> representa al número 43

    -> sucesor (Uno (Uno (Uno U))) -> representa al número 15
    Resultado: 10000 -> representa al númpero 16
-}

--Función que dados dos BinPos devuelva su suma.
sumaBin :: BinPos -> BinPos -> BinPos
sumaBin U b = (sucesor b)
sumaBin b U = (sucesor b)
sumaBin (Cero b) (Cero c) = (Cero (sumaBin b c))
sumaBin (Cero b) (Uno c) = (Uno (sumaBin b c))
sumaBin (Uno b) (Cero c) = (Uno (sumaBin b c))
sumaBin (Uno b) (Uno c) = (Cero (sumaBin (sucesor b) c))
{-ejemplos:
    -> sumaBin (Cero (Cero (Cero (Cero U)))) (Cero (Cero U)) -> representa la suma de 16 + 4 = 20
    Resultado: 10100 -> representa al número 20
    
    -> sumaBin (Cero (Uno (Uno (Cero U)))) (Cero (Cero (Cero U))) -> representa la suma de 22 + 8 = 30
    Resultado: 11110 -> representa al número 30
-}

--Función que dados dos BinPos devuelva su resta.
restaBin :: BinPos -> BinPos -> BinPos
restaBin U b = U
restaBin (Cero U) U = U
restaBin (Cero b) U = Uno (restaBin b U)
restaBin (Uno b) U = Cero b
restaBin (Cero b) (Cero c) = Cero (restaux b c)
restaBin (Uno b) (Uno c) = Cero (restaux b c)
restaBin (Cero b) (Uno c) = Uno (restaux b (sucesor c))
restaBin (Uno b) (Cero c) = Uno (restaux b c)
{-ejemplos:
    -> restaBin (Cero (Cero (Cero U))) (Uno U) -> representa la resta de 8 - 3 = 5
    Resultado: 101 -> representa al número 5
    -> restaBin (Cero (Uno (Cero U))) (Uno U) -> representa la resta de 11 -3 = 7
    Resultado: 111 -> representa al númpero 7

    Esta función al final no nos salio porque si quieres restar numeros binarios con mas de una cifra en su representacion decimal pasa algo tipo:

    -> restaBin (Uno (Uno (Cero U))) (Cero (Uno (Cero U))) -> representa la resta de 11 - 10 = 1
    Resultado: 1001 -> representa al número 9 (Lo cual es un error y falla la funcion :( )
-}

--Funcion auxiliar que acomoda la resta para tener resultados positivos 
restaux :: BinPos -> BinPos -> BinPos
restaux a b = if longBin a > longBin b then restaBin a b else restaBin b a

--Longitud de un número binario
longBin :: BinPos -> Int
longBin U = 1
longBin (Cero b) = 1 + (longBin b)
longBin (Uno b) = 1 + (longBin b)


--Función que dados dos BinPos devuelva su producto.
multiplica :: BinPos -> BinPos -> BinPos
multiplica U x = x 
multiplica x U = x 
multiplica (Cero x) (Cero y) = Cero (Cero (multiplica x y))
multiplica a@(Uno x) b@(Uno y) = sumaBin a (Cero (multiplica a y))
multiplica a@(Cero x) b@(Uno y) = sumaBin a (Cero (multiplica a y))
multiplica a@(Uno x) b@(Cero y) = multiplica b a 
{-ejemplos:
    -> multiplica (Cero (Cero (Cero U))) (Cero (Cero U)) -> representa a la multiplicación de 8 x 4 = 32
    Resultado: 100000 -> representa al número 32 

    -> multiplica (Cero (Uno (Uno U))) (Cero U) -> representa a la multiplicación de 14 x 2 = 28
    Resultado: 11100 -> representa al número 28
-} 
 
--Función que dado un BinPos devuelva el número natural representable bajo el tipo Int
binPosToInt :: BinPos -> Int
binPosToInt n =
    let long = longBin n in
        calculaEntero n (long-1) -- -> no pudimos terminarla bien :( 

--Función Auxiliar encargada de calcular un entero Dado el BinPos, 
-- recorre el BinPos y lo multiplica por la base 2 teniendo como exponente la Longutud -1, hasta llegar al caso Base.
calculaEntero :: BinPos -> Int -> Int
calculaEntero U n = 1
calculaEntero (Cero b) n = 0 + calculaEntero b (n-1)
calculaEntero (Uno b) n = (1*(2^n)) + calculaEntero b (n-1)

--Funcion auxiliar para calcular la potencia de un número
potencia :: Int -> Int -> Int
potencia n 0 = 1
potencia m n = m * (potencia m (n-1))

--Función que dado un número natural de tipo Int devuelva su representación bajo el tipo BinPos
intToBinPos :: Int -> BinPos
intToBinPos 1 = U
intToBinPos b 
      | mod b 2 == 0 = Cero (intToBinPos (div b 2)) 
      | otherwise = Uno (intToBinPos (div b 2))
{-ejemplos: 
    -> intToBinPos 54 
    Resultado: 110110
    
    ->intToBinPos 80
    Resultado 1010000
-}