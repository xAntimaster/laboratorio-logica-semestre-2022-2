--Equipo: 
--Giron Jimenez Emmanuel
--Alejandro Blancas Peralta 

module Coloracion where

--Definimos el tipo de "Color" con los siguientes constructores.
data Color =  Rojo | Amarillo | Verde | Azul deriving (Show, Eq)

--Definimos el tipo de dato "Balcanes" que define a los países que pertenecen a la península Balcanica.
data Balcanes  = Albania
               | Bulgaria
               | BosniayHerzegovina
               | Kosovo
               | Macedonia
               | Montenegro deriving (Show, Eq, Ord)

--Dos países son adyacentes cuando comparten frontera. Se define esa relación.
type Ady = [(Balcanes, Balcanes)]

adyacencias :: Ady
adyacencias = [ (Albania, Montenegro), (Albania, Kosovo), (Albania, Macedonia), (Bulgaria, Macedonia), (BosniayHerzegovina, Montenegro), (Kosovo, Macedonia), (Kosovo, Montenegro) ]


--Definimos una coloración como:
type Coloracion = [(Color, Balcanes)]

--Definimos una colorocacion
miColoracion :: Coloracion
miColoracion = [ (Amarillo, Albania), (Amarillo, Bulgaria), (Rojo, BosniayHerzegovina), (Verde, Kosovo), (Verde, Macedonia), (Amarillo, Montenegro) ] -- -> esto es una mala coloracion

--La función esBuena, la cual regresa True si la coloración recibida es buena respecto a la matriz de adyacencias y False en otro caso.
esBuena :: Ady -> Coloracion -> Bool
esBuena ady col =
    let 
      colores = [[ snd x | x <- col, fst x == Rojo],
                 [ snd x | x <- col, fst x == Verde],
                 [ snd x | x <- col, fst x == Amarillo],
                 [ snd x | x <- col, fst x == Azul]]
    in
      verifica ady (creaTodasLasTuplas $ filter ((>1) . length) colores)

{-Ejemplos de buena coloracion:
  -> (Rojo, Albania), (Amarillo, Bulgaria), (Rojo, BosniayHerzegovina), (Verde, Kosovo), (Azul, Macedonia), (Amarillo, Montenegro)

  Ejemplos de una mala coloracion:
  -> (Amarillo, Albania), (Amarillo, Bulgaria), (Rojo, BosniayHerzegovina), (Verde, Kosovo), (Verde, Macedonia), (Amarillo, Montenegro)

-}

--todas las tuplas que se crean a partir de una coloracion y un pais (Balcanes)
creaTodasLasTuplas [] = []
creaTodasLasTuplas (z:zs) = [(x,y) | x <- z, y <-z] ++ creaTodasLasTuplas zs

--Verifica que las tuplas sean parte de las adyacencias.
verifica :: Ady -> [(Balcanes, Balcanes)] -> Bool
verifica ady [] = True
verifica ady (x:xs)
  | elem x ady = False
  | otherwise = verifica ady xs

--Colores y paises para formar todas las coloraciones
colores = [Rojo, Amarillo, Verde, Azul]
paises = [Albania, Bulgaria, BosniayHerzegovina, Kosovo, Macedonia, Montenegro]

 --Función coloraciones, que calcula todas las coloracines buenas y completas respecto a la matriz de adyacencias recibida nas y completas respecto a la matriz de adyacencias recibida.
coloraciones :: Ady -> [Coloracion]
coloraciones ad = filter (esBuena ad) color
   where 
    color = [[(col1, bal1)] ++ 
            [(col2, bal2)] ++
            [(col3, bal3)] ++
            [(col4, bal4)] ++
            [(col5, bal5)] ++
            [(col6, bal6)]
            | col1 <- colores,
              col2 <- colores,
              col3 <- colores,
              col4 <- colores,
              col5 <- colores,
              col6 <- colores,
              bal1 <- paises,
              bal2 <- paises,
              bal3 <- paises,
              bal4 <- paises,
              bal5 <- paises,
              bal6 <- paises] -- ->Estos casos si aceptan "repetidos"
